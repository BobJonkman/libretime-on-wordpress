<?php
// Program : nowplaying.php
// Purpose : Display all the attributes available from LibreTime /api/live-info
// Author  : Bob Jonkman  bob@radiowaterloo.ca
// Date    : 27 November 2020
// Notes   : Based on "current-show-linked" shortcode

/* *************************************************************************
    Copyright &copy; (C) 2020  Bob Jonkman bjonkman@sobac.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
************************************************************************* */


$baseURL = "https://radiowaterloo.ca" ;                   // Base URL of WordPress site
$apiURL  = "http://libretime.soundfm.ca/api/live-info/" ; // LibreTime API

function getShowURL ($showName,$showURL)
{
  if ( "" == $showURL )
  {
    // Get the category slug for this show. Note the LibreTime show name must match the category slug!
    // Remove stuff in brackets eg. "(syndicated)", "(repeat)"
    $showName = preg_replace("/\(.*\)/", "", $showName) ;

    // Create a suitable slug from title
    $showURL = $baseURL . "/category/" . sanitize_title($showName) . "?tag=about" ;
  }

  return ($showURL) ;
}


function trackImgExist ($metadata)
{
  if ("" != $metadata->artwork )
    $imageURL = "<img src=\"" . html_entity_decode($metadata->artwork_url) . "\" alt=\"\" style=\"float:right; margin-left:3px; width:64px;\" />" ;
  else
    $imageURL = "" ;

  return ($imageURL) ;
}


/* ********************************* *\
** **                             ** **
** **     Code Starts Here        ** **
** **                             ** **
\* ********************************* */

$opts = array('http' =>
  array(
    'timeout' => 5
  )
);


$context  = stream_context_create($opts);
$liveinfo = json_decode(file_get_contents($apiURL, false, $context));

/* #####DEBUG#####
echo "\n<!-- #####DEBUG#####\n " ;
print_r ( $liveinfo) ;
echo "\n#####DEBUG##### -->\n" ;
/* #####DEBUG##### */

echo "<h2>Current Show</h2>" ;


echo "\n<h3><a href=\"" . getShowURL( $liveinfo->currentShow[0]->name, $liveinfo->currentShow[0]->url ) . "\">" ;
if ( "" != $liveinfo->currentShow[0]->image_path )
  {
    echo "\n<img src=\"" . $liveinfo->currentShow[0]->image_path . "\" alt=\"\" style=\"float:right; margin:0 0 .5em .5em; max-width:150px !important;\" />" ; // !important because somewhere another style  has "max-width:100% !important"
  }
echo $liveinfo->currentShow[0]->name . "</a></h3>" ;
echo "\n<p>" . strftime("%A %e %B %Y, %l:%M%P",strtotime($liveinfo->currentShow[0]->start_timestamp)) . " &ndash; " . strftime("%l:%M%P",strtotime($liveinfo->currentShow[0]->end_timestamp)) . "</p>" ;
echo "<p>" . htmlspecialchars_decode( $liveinfo->currentShow[0]->description ) . "</p>" ;

echo "\n<table summary=\"Track List\">\n<thead>" ;
echo "\n<tr><th>&nbsp;</th><th>Title | Artist | Album</th><th>Genre</th></tr>" ;
echo "\n</thead>\n<tbody style=\"font-size:100%; vertical-align:middle;\">" ;
echo "\n<tr>" ;
echo "<td>" . get_date_from_gmt($liveinfo->previous->starts,"g:i:sa") . "</td>" ; // uses WordPress date format, see https://wordpress.org/support/article/formatting-date-and-time/

echo "<td>" ;

  if ( "" == $liveinfo->previous->metadata->url )
    echo trackImgExist($liveinfo->previous->metadata) . $liveinfo->previous->metadata->track_title ;
  else
    echo "<a href=\"" . $liveinfo->previous->metadata->url . "\">" . trackImgExist($liveinfo->next->metadata) . $liveinfo->previous->metadata->track_title . "</a>" ;

echo " | <strong>" . $liveinfo->previous->metadata->artist_name . "</strong> " ;
echo " | <i>" .  $liveinfo->previous->metadata->album_title . "</i></td>" ;
echo "<td>" . $liveinfo->previous->metadata->genre . "</td>" ;
echo "</tr>\n" ;

echo "<tr>" ;
echo "<td>" . get_date_from_gmt($liveinfo->current->starts,"g:i:sa") . "</td>" ;
echo "<td>" ;

  if ( "" == $liveinfo->current->metadata->url )
    echo trackImgExist($liveinfo->current->metadata) . $liveinfo->current->metadata->track_title ;
  else
    echo "<a href=\"" . $liveinfo->current->metadata->url . "\">" . trackImgExist($liveinfo->current->metadata) . $liveinfo->current->metadata->track_title . "</a>" ;

echo " | <strong>" . $liveinfo->current->metadata->artist_name . "</strong> " ;
echo " | <i>" .  $liveinfo->current->metadata->album_title . "</i></td>" ;
echo "<td>" . $liveinfo->current->metadata->genre . "</td>" ;

echo "</tr>\n<tr>" ;

echo "<td>" . get_date_from_gmt($liveinfo->next->starts,"g:i:sa") . "</td>" ;
echo "<td>" ;

  if ( "" == $liveinfo->next->metadata->url )
    echo trackImgExist($liveinfo->next->metadata) . $liveinfo->next->metadata->track_title ;
  else
    echo "<a href=\"" . $liveinfo->next->metadata->url . "\">" . trackImgExist($liveinfo->next->metadata) . $liveinfo->next->metadata->track_title . "</a>" ;

echo " | <strong>" . $liveinfo->next->metadata->artist_name . "</strong> " ;
echo " | <i>" .  $liveinfo->next->metadata->album_title . "</i></td>" ;
echo "<td>" . $liveinfo->next->metadata->genre . "</td>" ;
echo "</tr>" ;

echo "</tbody></table>\n" ;

echo "\n<div style=\"margin-top:3ex; border-top:thick double black; padding-top:3ex; \">" ;
echo "<h2>Next Show</h2>" ;

echo "\n<h3><a href=\"" . getShowURL( $liveinfo->nextShow[0]->name, $liveinfo->nextShow[0]->url ) . "\">" ;
if ( "" != $liveinfo->nextShow[0]->image_path )
  {
    echo "\n<img src=\"" . $liveinfo->nextShow[0]->image_path . "\" alt=\"\" style=\"float:right; margin:0 0 .5em .5em; max-width:150px !important;\" />" ;
  }
echo $liveinfo->nextShow[0]->name . "</a></h3>" ;
echo "\n<p>" . strftime("%A %e %B %Y, %l:%M%P",strtotime($liveinfo->nextShow[0]->start_timestamp)) . " &ndash; " . strftime("%l:%M%P",strtotime($liveinfo->nextShow[0]->end_timestamp)) . "</p>" ;
echo "<p>" . htmlspecialchars_decode( $liveinfo->nextShow[0]->description ) . "</p>" ;
echo "</div>" ;

?>