= LibreTime on WordPress =

Here are some files to display the contents provided by the LibreTime public APIs:

* /api/week-info for schedules
* /api/live-info for "Now Playing" information

Two files are intended as shortcodes for use with the WordPress plugin [XYZ PHPcode Snippets]:https://xyzscripts.com/wordpress-plugins/insert-php-code-snippet/details

* schedule.php
* nowplaying.php

The third file, *oldshedules.php* is intended as a standalone PHP page, but embeds the code from *schedule.php*

All the code was written for use at [CKMS-FM 102.7 Radio Waterloo]:https://radiowaterloo.ca/ and there is much formatting and styling specific to Radio Waterloo. If you use these files you'll probably need to make some changes...

You can see working code or prototypes:

[Schedule]:https://radiowaterloo.ca/schedule/
[Now Playing]:https://radiowaterloo.ca/now-playing (to me moved to [Listen Live]:https://radiowaterloo.ca/listen/ )
[Old Schedules]:https://radiowaterloo.ca/oldschedules/

Enjoy!
--Bob Jonkman
bob@radiowaterloo.ca
bjonkman@sobac.com
