<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--
Program : oldschedules.php
Purpose : Select and display LibreTime schedule files
Author  : Bob Jonkman bob@radiowaterloo.ca
Date    : 7 September 2020
Notes   : Based on schedule.php, this file contains copied code (should be linked, really)
          This file is not intended to be a shortcode
          
/* *************************************************************************
    Copyright &copy; (C) 2020  Bob Jonkman bjonkman@sobac.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
************************************************************************* */
-->
  <head>
    <title>
      Old Schedule | CKMS 102.7 FM Radio Waterloo
    </title>

<style>
body
 { margin:0 15em;
   padding-bottom:5ex;
   background:#FAA;
 }

table
 { border-collapse:collapse;
   margin-left:auto;
   margin-right:auto;
 }

td,th
 { border: thin solid black;
   padding: .5em;
 }
</style>

  </head>
  <body style="margin:0 10em; background:#FCC;">
<a href="https://radiowaterloo.ca/" title="CKMS 102.7 FM Radio Waterloo"><img src="https://radiowaterloo.ca/wp-content/uploads/2017/05/ckmsLogoRectangle.png" alt="CKMS 102.7 FM Radio Waterloo (logo of sunflower)" style="float:right; width:25%; margin:.5em;" /></a> <h1 style="clear:none;">Radio Waterloo Old Schedule</h1>
<div  style="float:right; clear:right; width:150px; margin:0 0 1em 1em; padding:.5; border:thin solid black; background:white;"><table summary="Quick Chart">
<tr><td style="background:white;">CKMS Program</td></tr>
<tr><td style="background:yellow;">CKMS Replay #</td></tr>
<tr><td style="background:aqua;">PSAs &amp; Ads ^</td></tr>
<tr><td style="background:#2A2;">Syndicated %</td></tr>
<tr><td style="background:#2A2;">Available *</td></tr>
</table><p style="text-align:right; padding-right:1em; height:1ex;"><a href="#legend" title="Colour and Symbol Legend" style="color:black;">Legend</a></p></div>
<p>Looking to see what you've missed? That's OK, this is <strong>an old schedule</strong>! If you'd like to be up-to-date, check this week's <a href="https://radiowaterloo.ca/schedule/" title="Radio Waterloo Schedule | CKMS 102.7 FM">Radio Waterloo Schedule</a>.
</p>
<p>For a description of each show, please see the <a href="https://radiowaterloo.ca/category/shows/?tag=about" title="Shows | CKMS 102.7 FM Radio Waterloo">About Our Shows</a> page. For shows that are no longer on the current schedule see <a href="https://radiowaterloo.ca/category/off-the-air/?tag=about" title="Off The Air | CKMS 102.7 FM Radio Waterloo">Off The Air</a>.</p>


<?php

// Get the directory listing of .json files
$webroot = "/home/soundfm/public_html/" ;
$webdomain = "http://radiowaterloo.ca/" ;
$jsonfolder = "oldschedule/" ;   // gets appended to either $webroot or $webdomain


// sample code from https://www.php.net/manual/en/function.opendir.php
if (is_dir($webroot . $jsonfolder)) {
    if ($dh = opendir($webroot . $jsonfolder)) {
        while (($file = readdir($dh)) !== false) {
//            echo "filename: $file : filetype: " . filetype($dir . $file) . "\n";
            if (false !== strpos($file,".json")) { // filter file list
                $files[] = $file ;
            }
        }
        closedir($dh);
    }
}
rsort($files) ;  // reverse sort (newest to oldest)


echo "\n<!-- #####DEBUG#####\n" ;
echo "\n#####DEBUG##### \$files_GET= \n" ;
print_r($files) ; #####DEBUG#####
echo "\n#####DEBUG##### \$_GET= \n" ;
print_r($_GET) ; #####DEBUG#####
echo "\n#####DEBUG##### \$_GET[\"jsonfile\"]= " . $_GET["jsonfile"] ;
echo "\n#####DEBUG##### -->\n" ;

if ("" == $_GET["jsonfile"])
    $jsonfile = $files[0] ; // use first (newest) file if none defined
else
    $jsonfile = $_GET["jsonfile"] ;
echo "\n<!-- #####DEBUG##### \$jsonfile= " . $jsonfile . "-->\n" ;
?>

<form action="index.php" method="get">
  <label for="jsonfile">Pick Schedule File:</label>
  <select name="jsonfile" id="jsonfile">
<?php
  echo "\n<option value=\"" . $jsonfile . "\" selected=\"selected\">" . $jsonfile . "</option>" ;

  foreach($files as $file)
  {
    echo "\n<option value=\"" . $file . "\">" . $file . "</option>" ;
  }

?>
  </select>
  <label for="submit">&nbsp;</label>
  <input type="submit" value="Submit" name="submit" id="submit">
</form>


<?php

// Above the info block is not part of the ShortCode!

// current_time is a WordPress function; if we're not running under WordPress we define our own
//   *** Note that current_time() accounts for local timezone but date() does not!
if (  ! function_exists("current_time") )
  {
    echo "\n<!-- Defining current_time() -->\n" ;
    function current_time($timeformat)
      {
	if ("timestamp" == $timeformat) {
	  return time() ;
	} else {
          return date($timeformat) ;
	}
      }
   }

// sanitize_title() is a WordPress function...
if ( ! function_exists("sanitize_title") )
  {
    echo "\n<!-- Defining sanitize_title() -->\n" ;
    function sanitize_title($title)
      {
        return $title ; // Yes, I know, no sanitization is taking place.
      }
  }

/* ******************************************************* *\
**                                                         **
** Colour Airtime Schedule                                 **
** By Bob Jonkman   bob@radiowaterloo.ca                   **
** Date: 7 September 2020                                  **
** Copied from schedule.php with a few modifications       **
**                                                         **
**                                                         **
\* ******************************************************* */



$opts = array('http' =>
  array(
    'timeout' => 5
  )
);

$context  = stream_context_create($opts);


// For shortcode, define $jsonfile here
if ("" == $jsonfile )
    $jsonfile = "http://radiowaterloo.ca/oldschedule/lastweek-info.json" ;

$airtimeInfo = json_decode(file_get_contents($jsonfile, false, $context));


$previousDate = "" ;

$triggerstyle = array(
    "*" => "background:#2A2;   color:black;" ,   // Autopilot (lighter green)
    "#" => "background:yellow; color:black;" ,   // Replay
    "^" => "background:aqua;   color:black;" ,   // Ads & PSAs
    "%" => "background:#2A2;   color:black;" ,   // Syndicated
    "~" => "background:red;    color:white;"     // Pre-empted
) ;


$primetimewkend = array(
//   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23
    "·","·","·","·","·","·","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+" // Weekend hours for offpeak
) ;
$primetimewkday = array(
//   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23
    "·","·","·","·","·","·","+","+","+","-","-","-","-","-","+","+","+","+","+","+","+","+","+","+" // Weekday hours for offpeak
) ;

$primetime = array(
    $primetimewkend , // Sunday
    $primetimewkday , // Monday
    $primetimewkday , // Tuesday
    $primetimewkday , // Wednesday
    $primetimewkday , // Thursday
    $primetimewkday , // Friday
    $primetimewkend , // Saturday
) ;


$primetimestyle = array(
    "+" => "background:white;  color:black;",  // primetime
    "-" => "background:silver; color:black;",  // offpeak
    "·" => "background:gray;   color:black;",  // overnight
    "!" => "background:blue;   color:white;",  // special
) ;


// Make one table for entire schedule
echo "<table width=\"100%\" summary=\"Radio Waterloo Schedule\" style=\"border:thin solid black;\">";
echo "  <colgroup>";
echo "    <col style=\"width:11em;\">";
echo "  </colgroup>";


foreach($airtimeInfo as $day => $schedule) {


    foreach ($schedule as $show) {


        $showDateTime = explode(" ", $show->start_timestamp);
        $showDate = strtotime($showDateTime[0]) ;

        // This is how we roll (dates aren't available until the inner 'foreach($schedule as $show)'
        //  using current_time() from WordPress to get timezone correction from blog setting (php's date() returns UTC)

/* *******************
   **
   **  This section is omitted so we display dates in the past
   **
        if (strtotime(current_time( 'Y-m-d' )) > $showDate) {  // if today's date is greater than showdate
            break ; // break out of foreach($schedule as $show)
        }
   **
   **
   ****************** */


        // Check if date has changed to prevent <table> and date from displaying on every entry
        if ($previousDate != $showDate) {
           $previousDate = $showDate;

           echo "<tr><th colspan=\"2\" style=\"font-size:125%; font-weight:bolder; background:black; color:white;\">" . strftime("%A, %e %B %Y", $showDate) . "</th></tr>\n" ; // print date header
        }

        // Strip out apostrophe entity &#039; which causes false triggers on & and #
        $cleanshowname = preg_replace("/&#039;/", "", $show->name) ;

        // Check for an autopilot show, indicated by a key in array $trigger
        $showstyle = "background:white;   color:black;" ; //  class="program" ; // default styling

        foreach($triggerstyle as $triggerkey => $triggerstylevalue) {
            if (stripos($cleanshowname, $triggerkey) !== FALSE) {  // because stripos() may return 0
                $showstyle = $triggerstylevalue ;
                break ; // out of foreach($triggerstyle), don't loop for multiple triggers
            }
        }

/* ***********************************************************
   *****
   ***** Temporary; Don't link to "About" pages until we
   *****     find the WordPress function to link
   *****

        //   Check if a URL exists in the Airtime schedule, if so, use that instead
        if ($show->url == "") {

            // Generate the URL using category slug for this show. Note the Airtime show name must match the category slug!
            $showSlug = preg_replace("/\(.*\)/", "", $show->name) ; // Remove stuff in brackets eg. "(syndicated)", "(repeat)"

            // Create a suitable slug from title (sanitize_title() removes the class indicator #%*)
            $showSlug = sanitize_title($showSlug) ;

            //url for Show bio post using category and tag=about
            $showAboutURL = ("https://radiowaterloo.ca/category/" . $showSlug . "/?tag=about");
        } else {
            $showAboutURL = $show->url ;
        }

   *****
   *****
   *********************************************************** */

        // Check for an instance_description
        $showdescription="" ; // Clear previous show description in case this one is blank
        if ("" == $show->instance_description) {   // Check if an Instance Description exists
          $showdescription = $show->description ;  //   if so, use it
        } else {
          $showdescription = $show->instance_description ;
        }

        // Check for prime-time/off-peak
        $showdow  = strftime("%w", strtotime($show->starts)) ;
        $showhour = ltrim(strftime("%k", strtotime($show->starts))) ;
        $showprime = $primetime[$showdow][$showhour] ;
        $showprimestyle = $primetimestyle[$showprime] ;

        // Check for current time to highlight current show
        if ((current_time("timestamp") >= strtotime($show->starts)) and (current_time("timestamp") < strtotime($show->ends))) {  // "less than" because end times are exclusive
           $currentshowstyle = "font-weight:bolder; font-size:115%; border:thin solid orange;" ;
           $currenttimestyle = "font-weight:bolder; border:thin solid orange;" ;
        } else {
           $currentshowstyle = "" ;
           $currenttimestyle = "" ;
        }

        // Write the row for the show
        echo "<tr>" ;
        echo "<td style=\"" . $showprimestyle . $currenttimestyle . "\">" . ltrim(strftime("%l:%M%P", strtotime($show->starts))) . "&mdash;" . ltrim(strftime("%l:%M%P", strtotime($show->ends))) . "&nbsp;" . $showprime . "</td>" ;


// Temporarily don't link to "About" page
//        echo "<td style=\"" . $showstyle . $currentshowstyle . "\"><a href=\"" . $showAboutURL . "\"  style=\"color:inherit;\">" ;
        echo "<td style=\"" . $showstyle . $currentshowstyle . "\">" ;


/* *********************************************
        Image paths are to http://airtime.soundfm.ca resulting in mixed security content.
        I'm leaving this disabled until we have https://airtime.soundfm.ca Let's Encrypt enabled
           --Bob.

        // Add an icon if one is defined
        if ( $show->image_path != "" ) {
          echo "<img src=\"" . $show->image_path . "\" style=\"max-width:32px; max-height:32px; vertical-align:middle; margin-right:3px; float:left; \" />" ;
        }
// ********************************************* */


// Temporarily don't link to "About" page
//        echo $show->name . "</a>" ;
        echo $show->name ;

 if ("" != $showdescription) {
    // LibreTime encodes HTML chars, we have to undo that for our comments in description with htmlspecialchars_decode()
	  echo "<br /><span style=\"font-size:75%;\">" . htmlspecialchars_decode($showdescription) . "</span>" ;
        }

        echo "</td>" ;
        echo "</tr>\n" ;

    }  // foreach($schedule as $show)

}  // foreach($airtimeInfo as $day => $schedule)

      echo "</table>";

// EOF: ShortCode-colour_schedule.php

?>
<p><br /></p>
    <table summary="Schedule legend" id="legend" style="width:100%;">
      <colgroup>
       <col style="width:5.5em;"/>
       <col style="width:5.5em;"/>
      </colgroup>
      <thead>
        <tr style="background:black; color:white;">
          <th>Colour
          </th>
          <th>Symbol
          </th>
          <th>Meaning
          </th>
        </tr>
      </thead>
      <tbody>
        <tr style="background: white ">
          <td>White
          </td>
          <td style="text-align: center;">(none)
          </td>
          <td>Original CKMS programming
          </td>
        </tr>
        <tr style="background: yellow ">
          <td>Yellow
          </td>
          <td style="text-align: center;">#
          </td>
          <td>CKMS replayed shows
          </td>
        </tr>
         <tr style="background: aqua ">
          <td>Blue
          </td>
          <td style="text-align: center;">^
          </td>
          <td><a href="https://radiowaterloo.ca/how-to/advertise-on-ckms/#public-service-announcements" title="How To Advertise on CKMS | CKMS 102.7 FM Radio Waterloo">Public Service Announcements and Advertisements</a>
          </td>
        </tr>
       <tr style="background: #2A2;">
          <td>Green
          </td>
          <td style="text-align: center;">%
          </td>
          <td>Syndicated programs (shows produced at other radio stations). Available for new shows!
          </td>
        </tr>
        <tr style="background:#2A2; ">
          <td>Green
          </td>
          <td style="text-align: center;">*
          </td>
          <td>Available for new shows!
          </td>
        </tr>
        <tr style="background:red; color:white;">
          <td>Red</td>
          <td style="text-align: center;">~</td>
          <td>Pre-empted - The regularly scheduled show has been pre-empted for a special presentation</td>
        </tr>
        <tr style="color:black; background:white;">
           <td >White times</td>
           <td style="text-align: center;">&nbsp;+</td>
           <td>Prime time hours (6am-9am and 2pm-midnight weekdays, 6am-midnight weekends)</td>
        </tr>
        <tr style="color:black; background:silver;">
           <td >Shaded times</td>
           <td style="text-align: center;">&nbsp;-</td>
           <td style="color:black; background:white;">Off-peak hours (9am-2pm weekdays)</td>
        </tr>
        <tr style="color:black; background:grey;">
           <td >Dark shaded times</td>
           <td style="text-align: center;">&nbsp;&middot;</td>
           <td style="color:black; background:white;">Off-peak, overnight (midnight-6am daily)</td>
        </tr>
      </tbody>
    </table>

</body>
</html>
