<?php
/* ******************************************************* *\
**                                                         **
** Colour Airtime Schedule                                 **
** By Bob Jonkman   bob@radiowaterloo.ca                   **
** Based on [rolling_schedule]                             **
** Date: Sunday 22 December 2019                           **
**         - Added symbols for classes of shows            **
**           # Replay shows                                **
**           % Syndicated Shows                            **
**           * Available timeslots                         **
** Date: 11 Feb 2020, Nat                                  **
**         - changed syndicated NG colour from aqua to 2A2 **
**       9 March 2020 --Bob                                **
**         - Added class "psa-ads", symbol ^, colour aqua  **
**       24 March 2020 --Bob                               **
**         - added $cleanshowname to remove html entity    **
**       10 May 2020 --Bob                                 **
**         - added $currentshowstyle for current show      **
**       10 June 2020 --Bob                                **
**         - added $showdescription as subtext             **
**         - added $show->imagepath as icon (disabled)     **
**       29 July 2020 --Bob                                **
**         - create timeslot "Overnight" (dark grey)       **
**           (not subject to CanCon or SOCAN requirements) **
**         - Change "Pre-empted" symbol to "~"             **
**           (removes the "money" connotation)             **
**       1 December 2020 --Bob.                            **
**         - Activated $show-imagepath (show logos)        **
**                                                         **
**                                                         **
\* ******************************************************* */

/* *************************************************************************
    Copyright &copy; (C) 2020  Bob Jonkman bjonkman@sobac.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
************************************************************************* */


$opts = array('http' =>
  array(
    'timeout' => 5
  )
);

$context  = stream_context_create($opts);

$airtimeInfo = json_decode(file_get_contents("http://airtime.soundfm.ca/api/week-info/", false, $context));



$previousDate = "" ;

$triggerstyle = array(
    "*" => "background:#2A2;   color:black;" ,   // Autopilot (lighter green)
    "#" => "background:yellow; color:black;" ,   // Replay
    "^" => "background:aqua;   color:black;" ,   // Ads & PSAs
    "%" => "background:#2A2;   color:black;" ,   // Syndicated
    "~" => "background:red;    color:white;"     // Pre-empted
) ;


$primetimewkend = array(
//   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23
    "·","·","·","·","·","·","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+","+" // Weekend hours for offpeak
) ;
$primetimewkday = array(
//   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23
    "·","·","·","·","·","·","+","+","+","-","-","-","-","-","+","+","+","+","+","+","+","+","+","+" // Weekday hours for offpeak
) ;

$primetime = array(
    $primetimewkend , // Sunday
    $primetimewkday , // Monday
    $primetimewkday , // Tuesday
    $primetimewkday , // Wednesday
    $primetimewkday , // Thursday
    $primetimewkday , // Friday
    $primetimewkend , // Saturday
) ;


$primetimestyle = array(
    "+" => "background:white;  color:black;",  // primetime
    "-" => "background:silver; color:black;",  // offpeak
    "·" => "background:gray;   color:black;",  // overnight
    "!" => "background:blue;   color:white;",  // special
) ;


// Make one table for entire schedule
echo "<table width=\"100%\" summary=\"Radio Waterloo Schedule\">";
echo "  <colgroup>";
echo "    <col style=\"width:11em;\">";
echo "  </colgroup>";


foreach($airtimeInfo as $day => $schedule) {



    foreach ($schedule as $showkey => $show) {


        $showDateTime = explode(" ", $show->start_timestamp);
        $showDate = strtotime($showDateTime[0]) ;

        // This is how we roll (dates aren't available until the inner 'foreach($schedule as $show)'
        //  using current_time() from WordPress to get timezone correction from blog setting (php's date() returns UTC)



        if (strtotime(current_time( 'Y-m-d' )) > $showDate) {  // if today's date is greater than showdate
            break ; // break out of foreach($schedule as $show)
        }


        // Check if date has changed to prevent <table> and date from displaying on every entry
        if ($previousDate != $showDate) {
           $previousDate = $showDate;

           echo "<tr><th colspan=\"2\" style=\"font-size:125%; font-weight:bolder; background:black; color:white;\">" . strftime("%A, %e %B %Y", $showDate) . "</th></tr>\n" ; // print date header
        }

        // Strip out apostrophe entity &#039; which causes false triggers on & and #
        $cleanshowname = preg_replace("/&#039;/", "", $show->name) ;

        // Check for an autopilot show, indicated by a key in array $trigger
        $showstyle = "background:white;   color:black;" ; //  class="program" ; // default styling

        foreach($triggerstyle as $triggerkey => $triggerstylevalue) {
            if (stripos($cleanshowname, $triggerkey) !== FALSE) {  // because stripos() may return 0
                $showstyle = $triggerstylevalue ;
                break ; // out of foreach($triggerstyle), don't loop for multiple triggers
            }
        }

        //   Check if a URL exists in the Airtime schedule, if so, use that instead
        if ($show->url == "") {

            // Generate the URL using category slug for this show. Note the Airtime show name must match the category slug!
            $showSlug = preg_replace("/\(.*\)/", "", $show->name) ; // Remove stuff in brackets eg. "(syndicated)", "(repeat)"

            // Create a suitable slug from title (sanitize_title() removes the class indicator #%*)
            $showSlug = sanitize_title($showSlug) ;

            //url for Show bio post using category and tag=about
            $showAboutURL = ("https://radiowaterloo.ca/category/" . $showSlug . "/?tag=about");
        } else {
            $showAboutURL = $show->url ;
        }

        // Check for an instance_description
        $showdescription="" ; // Clear previous show description in case this one is blank
        if ("" == $show->instance_description) {   // Check if an Instance Description exists
          $showdescription = $show->description ;  //   if so, use it
        } else {
          $showdescription = $show->instance_description ;
        }

        // Check for prime-time/off-peak
        $showdow  = strftime("%w", strtotime($show->starts)) ;
        $showhour = ltrim(strftime("%k", strtotime($show->starts))) ;
        $showprime = $primetime[$showdow][$showhour] ;
        $showprimestyle = $primetimestyle[$showprime] ;

        // Check for current time to highlight current show
        if ((current_time("timestamp") >= strtotime($show->starts)) and (current_time("timestamp") < strtotime($show->ends))) {  // "less than" because end times are exclusive
           $currentshowstyle = "font-weight:bolder; font-size:115%; border:thin solid orange;" ;
           $currenttimestyle = "font-weight:bolder; border:thin solid orange;" ;
        } else {
           $currentshowstyle = "" ;
           $currenttimestyle = "" ;
        }

        // Write the row for the show
        echo "<tr>" ;
        echo "<td style=\"" . $showprimestyle . $currenttimestyle . "\">" . ltrim(strftime("%l:%M%P", strtotime($show->starts))) . "&mdash;" . ltrim(strftime("%l:%M%P", strtotime($show->ends))) . "&nbsp;" . $showprime . "</td>" ;
        echo "<td style=\"" . $showstyle . $currentshowstyle . "\"><a href=\"" . $showAboutURL . "\"  style=\"color:inherit;\">" ;


/* *********************************************
        Image paths are to http://airtime.soundfm.ca resulting in mixed security content.
        I'm leaving this disabled until we have https://airtime.soundfm.ca Let's Encrypt enabled
           --Bob.

        1 December 2020 : Enabled without httpS ; alt="" prevents placeholders for mixed-security images on secure browsers --Bob.
// ********************************************* */

        // Add an icon if one is defined
        if ( $show->image_path != "" ) {
          echo "<img src=\"" . $show->image_path . "\" alt=\"\" style=\"max-height:48px; vertical-align:middle; margin-right:3px; float:right; \" />" ;
        }

        echo $show->name . "</a>" ;

 if ("" != $showdescription) {
    // LibreTime encodes HTML chars, we have to undo that for our comments in description with htmlspecialchars_decode()
	  echo "<br /><span style=\"font-size:75%;\">" . htmlspecialchars_decode($showdescription) . "</span>" ;
        }

        echo "</td>" ;
        echo "</tr>\n" ;


    }  // foreach($schedule as $show)


}  // foreach($airtimeInfo as $day => $schedule)

      echo "</table>";

// EOF: ShortCode-colour_schedule.php
